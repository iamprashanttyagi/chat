var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function (req, res) {
    res.sendfile('./view/index.html');
});
users = [];
io.on('connection', function (socket) {
    console.log("new user connected ..... :)")

    socket.on('newUser', function (data) {
        if (users.indexOf(data) > -1) {
            socket.emit('userExist', "Username already exist.... try to change");
        } else {
            users.push(data);
            io.sockets.emit('newJoin', data)
            socket.emit('username', {
                username: data
            })
        }
    });

    socket.on('send_msg', function (data) {
        io.sockets.emit('message', data)
    })

    socket.on('disconnect', function () {
        console.log("user disconnected ..... :( ")
    });

});

http.listen(9000, function () {
    console.log('listening on localhost:9000');
});